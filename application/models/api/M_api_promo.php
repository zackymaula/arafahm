<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_promo extends CI_Model {

	public function GetPromo()
	{
		$this->db->select('*');
		$this->db->from('c_promo');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_promo' => (int)$data['id_promo'],
				'promo_name' => $data['promo_name'],
				'promo_price_hk' => $data['promo_price_hk'],
				'promo_price_tw' => strval($data['promo_price_hk']*4),
				'promo_price_sg' => strval($data['promo_price_hk']*0.2),
				'promo_price_my' => strval($data['promo_price_hk']*0.6),
				'promo_file_path' => $data['promo_file_path'],
				'promo_file_name' => $data['promo_file_name'],
				'promo_description' => $data['promo_description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

}
