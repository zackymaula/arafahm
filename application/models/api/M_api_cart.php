<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_cart extends CI_Model {

	//GET ID MEMBER
	public function GetIdMember($member_generate_id,$member_phone)
	{
		$arrayWhere = array(
			'member_generate_id' => $member_generate_id,
			'member_phone' => $member_phone
		);

		$this->db->select('id_member');
		$this->db->from('u_members');
		$this->db->where($arrayWhere);
		$query = $this->db->get()->row()->id_member;
		return $query;
		//return $query->result_array();
	}

  //GET LIST CART PER MEMBER || GET FROM PRODUCT AND PROMO
	public function GetCartPerMember($id_member)
	{
    $this->db->select('p.*, ca.*');
		$this->db->from('p_products p');
    $this->db->join('t_cart ca', 'ca.id_product = p.id_products');
		$this->db->order_by('ca.created_at', 'desc');
		$this->db->where('ca.id_member', $id_member);
		$queryProduct = $this->db->get();
		$dataArrayProduct = $queryProduct->result_array();

		$this->db->select('pr.*, ca.*');
		$this->db->from('c_promo pr');
    $this->db->join('t_cart ca', 'ca.id_promo = pr.id_promo');
		$this->db->order_by('ca.created_at', 'desc');
		$this->db->where('ca.id_member', $id_member);
		$queryPromo = $this->db->get();
		$dataArrayPromo = $queryPromo->result_array();


    if ($queryProduct->num_rows()>=1 || $queryPromo->num_rows()>=1) {
      foreach ($dataArrayProduct as $dataProduct) {
  			$dataArrayConvert[] = array(
          'id_cart' => (int)$dataProduct['id_cart'],
          'id_member' => (int)$dataProduct['id_member'],
  				'id_products' => (int)$dataProduct['id_products'],
  				'product_name' => $dataProduct['product_name'],
  				'product_price_hk' => $dataProduct['product_price_hk'],
  				'product_price_tw' => strval($dataProduct['product_price_hk']*4),
  				'product_price_sg' => strval($dataProduct['product_price_hk']*0.2),
  				'product_price_my' => strval($dataProduct['product_price_hk']*0.6),
  				'product_file_path' => $dataProduct['product_file_path'],
  				'product_file_name' => $dataProduct['product_file_name'],
  				'description' => $dataProduct['description'],
  			);
  		};

  		foreach ($dataArrayPromo as $dataPromo) {
  			$dataArrayConvert[] = array(
          'id_cart' => (int)$dataPromo['id_cart'],
          'id_member' => (int)$dataPromo['id_member'],
  				'id_products' => (int)$dataPromo['id_promo'],
  				'product_name' => $dataPromo['promo_name'],
  				'product_price_hk' => $dataPromo['promo_price_hk'],
  				'product_price_tw' => strval($dataPromo['promo_price_hk']*4),
  				'product_price_sg' => strval($dataPromo['promo_price_hk']*0.2),
  				'product_price_my' => strval($dataPromo['promo_price_hk']*0.6),
  				'product_file_path' => $dataPromo['promo_file_path'],
  				'product_file_name' => $dataPromo['promo_file_name'],
  				'description' => $dataPromo['promo_description'],
  			);
  		};

  		$result = array(
  			'data' => $dataArrayConvert,
  		);
    } else {
      $result = array(
  			'data' => [],
  		);
    }

		return $result;
	}

  //INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		//$insert_id = $this->db->insert_id();
		return $res;
	}

	public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

}
