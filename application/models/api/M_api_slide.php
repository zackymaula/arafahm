<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_slide extends CI_Model {

	public function GetSlide()
	{
		$this->db->select('*');
		$this->db->from('c_sliders');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_slide' => (int)$data['id_slide'],
				'title' => $data['title'],
				'file_path' => $data['file_path'],
				'file_name' => $data['file_name'],
				'description' => $data['description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

}