<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_paket extends CI_Model {

	public function GetPaket()
	{
		$this->db->select('*');
		$this->db->from('c_paket');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_paket' => (int)$data['id_paket'],
				'file_path' => $data['file_path'],
				'file_name' => $data['file_name'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

}