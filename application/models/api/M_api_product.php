<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_product extends CI_Model {

	//GET ALL / LIST
	public function GetProductAll()
	{
		$this->db->select('p.*,
							c.id_categories,
							cs.id_category_sub,
    						c.name as category_name,
    						cs.name as categorysub_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.created_at', 'desc');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_products' => (int)$data['id_products'],
				'id_categories' => (int)$data['id_categories'],
				'category_name' => $data['category_name'],
				'id_category_sub' => (int)$data['id_category_sub'],
				'categorysub_name' => $data['categorysub_name'],
				'product_name' => $data['product_name'],
				'product_price_hk' => $data['product_price_hk'],
				'product_price_tw' => strval($data['product_price_hk']*4),
				'product_price_sg' => strval($data['product_price_hk']*0.2),
				'product_price_my' => strval($data['product_price_hk']*0.6),
				'product_file_path' => $data['product_file_path'],
				'product_file_name' => $data['product_file_name'],
				'description' => $data['description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

	//GET ALL / LIMIT
	public function GetProductAllLimit()
	{
		$this->db->select('p.*,
							c.id_categories,
							cs.id_category_sub,
    						c.name as category_name,
    						cs.name as categorysub_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->limit(15, 0);
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.created_at', 'desc');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_products' => (int)$data['id_products'],
				'id_categories' => (int)$data['id_categories'],
				'category_name' => $data['category_name'],
				'id_category_sub' => (int)$data['id_category_sub'],
				'categorysub_name' => $data['categorysub_name'],
				'product_name' => $data['product_name'],
				'product_price_hk' => $data['product_price_hk'],
				'product_price_tw' => strval($data['product_price_hk']*4),
				'product_price_sg' => strval($data['product_price_hk']*0.2),
				'product_price_my' => strval($data['product_price_hk']*0.6),
				'product_file_path' => $data['product_file_path'],
				'product_file_name' => $data['product_file_name'],
				'description' => $data['description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

	//GET ONE DETAIL
	public function GetProductDetail($id_products)
	{
		$this->db->select('p.*,
							c.id_categories,
							cs.id_category_sub,
    						c.name as category_name,
    						cs.name as categorysub_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->group_by('p.id_products');
		$this->db->where('p.id_products', $id_products);
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_products' => (int)$data['id_products'],
				'id_categories' => (int)$data['id_categories'],
				'category_name' => $data['category_name'],
				'id_category_sub' => (int)$data['id_category_sub'],
				'categorysub_name' => $data['categorysub_name'],
				'product_name' => $data['product_name'],
				'product_price_hk' => $data['product_price_hk'],
				'product_price_tw' => strval($data['product_price_hk']*4),
				'product_price_sg' => strval($data['product_price_hk']*0.2),
				'product_price_my' => strval($data['product_price_hk']*0.6),
				'product_file_path' => $data['product_file_path'],
				'product_file_name' => $data['product_file_name'],
				'description' => $data['description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

	//PER CATEGORY
	public function GetProductPerCategory($id_categories)
	{
		$this->db->select('p.*,
							c.id_categories,
							cs.id_category_sub,
    						c.name as category_name,
    						cs.name as categorysub_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.created_at', 'desc');
		$this->db->where('c.id_categories', $id_categories);
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_products' => (int)$data['id_products'],
				'id_categories' => (int)$data['id_categories'],
				'category_name' => $data['category_name'],
				'id_category_sub' => (int)$data['id_category_sub'],
				'categorysub_name' => $data['categorysub_name'],
				'product_name' => $data['product_name'],
				'product_price_hk' => $data['product_price_hk'],
				'product_price_tw' => strval($data['product_price_hk']*4),
				'product_price_sg' => strval($data['product_price_hk']*0.2),
				'product_price_my' => strval($data['product_price_hk']*0.6),
				'product_file_path' => $data['product_file_path'],
				'product_file_name' => $data['product_file_name'],
				'description' => $data['description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

	//PER CATEGORY SUB
	public function GetProductPerCategorySub($id_categories, $id_category_sub)
	{
		$this->db->select('p.*,
							c.id_categories,
							cs.id_category_sub,
    						c.name as category_name,
    						cs.name as categorysub_name');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.created_at', 'desc');
		$this->db->where('c.id_categories', $id_categories);
		$this->db->where('cs.id_category_sub', $id_category_sub);
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_products' => (int)$data['id_products'],
				'id_categories' => (int)$data['id_categories'],
				'category_name' => $data['category_name'],
				'id_category_sub' => (int)$data['id_category_sub'],
				'categorysub_name' => $data['categorysub_name'],
				'product_name' => $data['product_name'],
				'product_price_hk' => $data['product_price_hk'],
				'product_price_tw' => strval($data['product_price_hk']*4),
				'product_price_sg' => strval($data['product_price_hk']*0.2),
				'product_price_my' => strval($data['product_price_hk']*0.6),
				'product_file_path' => $data['product_file_path'],
				'product_file_name' => $data['product_file_name'],
				'description' => $data['description'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

}
