<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_auth extends CI_Model {

	//AUTH MOBILE
	public function GetMembers($member_generate_id,$member_phone)
	{
		$arrayWhere = array(
			'member_generate_id' => $member_generate_id,
			'member_phone' => $member_phone
		);

		$this->db->select('*');
		$this->db->from('u_members');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
		//return $query->result_array();
	}

	//CHECK MEMBER ADA ATAU TIDAK
	public function CheckMembers($member_phone)
	{
		$this->db->select('*');
		$this->db->from('u_members');
		$this->db->where('member_phone', $member_phone);
		$query = $this->db->get();
		return $query;
	}

	//CHECK MEMBER ADA ATAU TIDAK
	// public function CheckMembers($member_generate_id,$member_phone)
	// {
	// 	$arrayWhere = array(
	// 		'member_generate_id' => $member_generate_id,
	// 		'member_phone' => $member_phone
	// 	);
	//
	// 	$this->db->select('*');
	// 	$this->db->from('u_members');
	// 	$this->db->where($arrayWhere);
	// 	$query = $this->db->get();
	// 	if ($this->db->affected_rows()) {
	// 		$status = 'true'; //JIKA ADA
	// 	} else {
	// 		$status = 'false'; //JIKA TIDAK
	// 	}
	//
	// 	$result = array(
	// 		'status' => $status,
	// 	);
	//
	// 	return $result;
	// }

	//INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	//UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}

}
