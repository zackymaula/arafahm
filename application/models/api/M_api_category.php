<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_category extends CI_Model {

	public function GetCategory()
	{
		$this->db->select('*');
		$this->db->from('p_categories');
		$this->db->order_by('id_categories', 'asc');
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_categories' => (int)$data['id_categories'],
				'name' => $data['name'],
				'file_path' => $data['file_path'],
				'file_name' => $data['file_name'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

	public function GetCategorySub($id_categories)
	{
		$this->db->select('c.id_categories,
							cs.id_category_sub,
    						c.name as category_name, 
    						cs.name as categorysub_name,
    						cs.file_path,
    						cs.file_name');
		$this->db->from('p_category_sub cs');
		$this->db->join('p_categories c', 'cs.id_categories = c.id_categories');
		$this->db->where('cs.id_categories', $id_categories);
		$query = $this->db->get();
		$dataArray = $query->result_array();

		foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id_categories' => (int)$data['id_categories'],
				'id_category_sub' => (int)$data['id_category_sub'],
				'category_name' => $data['category_name'],
				'categorysub_name' => $data['categorysub_name'],
				'file_path' => $data['file_path'],
				'file_name' => $data['file_name'],
			);
		};

		$result = array(
			'data' => $dataArrayConvert,
		);

		return $result;
	}

}