<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Category extends CI_Model {

	public function GetAllCategory()
	{
		$this->db->select('*');
		$this->db->from('p_categories');
		$query = $this->db->get();
		return $query->result_array();
	}
    
	public function GetAllCategorySub()
	{
		$this->db->select('cs.id_category_sub,
							c.name as c_name, 
							cs.name as cs_name,
							cs.file_path,
							cs.file_name');
		$this->db->from('p_category_sub cs');
		$this->db->join('p_categories c', 'c.id_categories = cs.id_categories');
		$this->db->order_by('c.id_categories', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

}