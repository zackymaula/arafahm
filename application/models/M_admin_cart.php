<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Cart extends CI_Model {

  //CART LIST
	public function GetCartList()
	{
    $this->db->select('p.*, m.*, ca.created_at as date');
		$this->db->from('p_products p');
    $this->db->join('t_cart ca', 'ca.id_product = p.id_products');
    $this->db->join('u_members m', 'm.id_member = ca.id_member');
		$this->db->order_by('ca.created_at', 'desc');
		$queryProduct = $this->db->get();
		$dataArrayProduct = $queryProduct->result_array();

		$this->db->select('pr.*, m.*, ca.created_at as date');
		$this->db->from('c_promo pr');
    $this->db->join('t_cart ca', 'ca.id_promo = pr.id_promo');
    $this->db->join('u_members m', 'm.id_member = ca.id_member');
		$this->db->order_by('ca.created_at', 'desc');
		$queryPromo = $this->db->get();
		$dataArrayPromo = $queryPromo->result_array();


    if ($queryProduct->num_rows()>=1 || $queryPromo->num_rows()>=1) {
      foreach ($dataArrayProduct as $dataProduct) {
  			$dataArrayConvert[] = array(
          'id_member' => (int)$dataProduct['id_member'],
          'member_name' => $dataProduct['member_name'],
          'member_phone' => $dataProduct['member_phone'],
  				'id_products' => (int)$dataProduct['id_products'],
  				'product_name' => $dataProduct['product_name'],
  				'product_price_hk' => $dataProduct['product_price_hk'],
  				'product_file_path' => $dataProduct['product_file_path'],
  				'product_file_name' => $dataProduct['product_file_name'],
  				'description' => $dataProduct['description'],
  				'date' => $dataProduct['date'],
  			);
  		};

  		foreach ($dataArrayPromo as $dataPromo) {
  			$dataArrayConvert[] = array(
          'id_member' => (int)$dataPromo['id_member'],
          'member_name' => $dataPromo['member_name'],
          'member_phone' => $dataPromo['member_phone'],
  				'id_products' => (int)$dataPromo['id_promo'],
  				'product_name' => '[PROMO] '.$dataPromo['promo_name'],
  				'product_price_hk' => $dataPromo['promo_price_hk'],
  				'product_file_path' => $dataPromo['promo_file_path'],
  				'product_file_name' => $dataPromo['promo_file_name'],
  				'description' => $dataPromo['promo_description'],
  				'date' => $dataPromo['date'],
  			);
  		};

  	  $result = $dataArrayConvert;
    } else {
      $result = [];
    }

		return $result;
	}

}
