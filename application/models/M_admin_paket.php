<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Paket extends CI_Model {

    //ADMIN LIST
	public function GetPaket()
	{
		$this->db->select('*');
		$this->db->from('c_paket');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//UPDATE DETAIL
	public function GetPaketDetail($id_paket)
	{
		$this->db->select('*');
		$this->db->from('c_paket');
		$this->db->where('id_paket', $id_paket);
		$query = $this->db->get();
		return $query->result_array();
	}
    
    //ADMIN INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//ADMIN DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//ADMIN UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
    }
    
}