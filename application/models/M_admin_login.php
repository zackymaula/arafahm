<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Login extends CI_Model {

	//ADMIN BACK
	public function GetAdminUsers($usernm,$passwd)
	{
		$arrayWhere = array(
			'usernm' => $usernm, 
			'passwd' => $passwd
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
		//return $query->result_array();
	}

}