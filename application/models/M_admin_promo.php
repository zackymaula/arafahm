<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Promo extends CI_Model {

    //ADMIN LIST
	public function GetPromo()
	{
		$this->db->select('*');
		$this->db->from('c_promo');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN UPDATE DETAIL
	public function GetPromoDetail($id_promo)
	{
		$this->db->select('*');
		$this->db->from('c_promo');
		$this->db->where('id_promo', $id_promo);
		$query = $this->db->get();
		return $query->result_array();
    }
    
    //ADMIN INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//ADMIN DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//ADMIN UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
    }
    
}