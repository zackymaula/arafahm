<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Member extends CI_Model {

  //MEMBER LIST
	public function GetMemberList()
	{
		$this->db->select('*');
		$this->db->from('u_members');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//MEMBER ONE
	public function GetMember($id_member)
	{
		$this->db->select('*');
		$this->db->from('u_members');
		$this->db->where('id_member', $id_member);
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function GetCharPerMember($id_member)
	{
		$this->db->select('p.*, ca.created_at as date');
		$this->db->from('p_products p');
    $this->db->join('t_cart ca', 'ca.id_product = p.id_products');
		$this->db->order_by('ca.created_at', 'desc');
		$this->db->where('ca.id_member', $id_member);
		$queryProduct = $this->db->get();
		$dataArrayProduct = $queryProduct->result_array();

		$this->db->select('pr.*, ca.created_at as date');
		$this->db->from('c_promo pr');
    $this->db->join('t_cart ca', 'ca.id_promo = pr.id_promo');
		$this->db->order_by('ca.created_at', 'desc');
		$this->db->where('ca.id_member', $id_member);
		$queryPromo = $this->db->get();
		$dataArrayPromo = $queryPromo->result_array();


    if ($queryProduct->num_rows()>=1 || $queryPromo->num_rows()>=1) {
      foreach ($dataArrayProduct as $dataProduct) {
  			$dataArrayConvert[] = array(
  				'id_products' => (int)$dataProduct['id_products'],
  				'product_name' => $dataProduct['product_name'],
  				'product_price_hk' => $dataProduct['product_price_hk'],
  				'product_file_path' => $dataProduct['product_file_path'],
  				'product_file_name' => $dataProduct['product_file_name'],
  				'description' => $dataProduct['description'],
  				'date' => $dataProduct['date'],
  			);
  		};

  		foreach ($dataArrayPromo as $dataPromo) {
  			$dataArrayConvert[] = array(
  				'id_products' => (int)$dataPromo['id_promo'],
  				'product_name' => '[PROMO] '.$dataPromo['promo_name'],
  				'product_price_hk' => $dataPromo['promo_price_hk'],
  				'product_file_path' => $dataPromo['promo_file_path'],
  				'product_file_name' => $dataPromo['promo_file_name'],
  				'description' => $dataPromo['promo_description'],
  				'date' => $dataPromo['date'],
  			);
  		};

  	  $result = $dataArrayConvert;
    } else {
      $result = [];
    }

		return $result;
	}
}
