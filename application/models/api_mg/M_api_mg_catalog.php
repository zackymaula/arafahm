<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_mg_catalog extends CI_Model {

  //MAGENTO CATALOG ALL
  public function GetMagentoCatalogAll()
  {
    $db = $this->load->database('db_magento', TRUE);

    //$query = $db->query("SELECT * FROM mg_catalog_full");
    //$category = 'Laptop';

    $db->select('*');
    $db->from('mg_catalog_full');
    $query = $db->get();
    $dataArray = $query->result_array();

    foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id' => $data['id'],
				'name' => $data['name'],
        'sku' => $data['sku'],
        'price' => explode(".",$data['price'])[0],
        'description' => $data['description'],
        'image' => $data['image'],
        'categorynames' => explode(",",$data['category_names'])[0],
        'categorysubnames' => explode(",",$data['category_names'])[1],
			);
		};

    $result = array(
			'data' => $dataArrayConvert,
		);

    return $result;
  }

  //MAGENTO CATALOG CATEGORY
  public function GetMagentoCatalogCategory($category='')
  {
    $db = $this->load->database('db_magento', TRUE);

    $db->select('*');
    $db->from('mg_catalog_full');
    $db->like('category_names', $category);
    $query = $db->get();
    $dataArray = $query->result_array();

    foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id' => $data['id'],
				'name' => $data['name'],
        'sku' => $data['sku'],
        'price' => explode(".",$data['price'])[0],
        'description' => $data['description'],
        'image' => $data['image'],
        'categorynames' => explode(",",$data['category_names'])[0],
        'categorysubnames' => explode(",",$data['category_names'])[1],
			);
		};

    $result = array(
			'data' => $dataArrayConvert,
		);

    return $result;
  }

  //MAGENTO CATALOG CATEGORY SUB
  public function GetMagentoCatalogCategorySub($category='',$categorysub='')
  {
    $db = $this->load->database('db_magento', TRUE);

    $db->select('*');
    $db->from('mg_catalog_full');
    $db->like('category_names', $category);
    $db->like('category_names', $categorysub);
    $query = $db->get();
    $dataArray = $query->result_array();

    foreach ($dataArray as $data) {
			$dataArrayConvert[] = array(
				'id' => $data['id'],
				'name' => $data['name'],
        'sku' => $data['sku'],
        'price' => explode(".",$data['price'])[0],
        'description' => $data['description'],
        'image' => $data['image'],
        'categorynames' => explode(",",$data['category_names'])[0],
        'categorysubnames' => explode(",",$data['category_names'])[1],
			);
		};

    $result = array(
			'data' => $dataArrayConvert,
		);

    return $result;
  }

}
