<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_api_mg_cart extends CI_Model {

	//GET ID MEMBER
	public function GetIdMember($member_generate_id,$member_phone)
	{
		$arrayWhere = array(
			'member_generate_id' => $member_generate_id,
			'member_phone' => $member_phone
		);

		$this->db->select('id_member');
		$this->db->from('u_members');
		$this->db->where($arrayWhere);
		$query = $this->db->get()->row()->id_member;
		return $query;
	}

	//GET LIST CART PER MEMBER
	public function GetCartPerMember($id_member)
	{
		$this->db->select('*');
		$this->db->from('t_cart_mg');
		$this->db->order_by('created_at', 'desc');
		$this->db->where('id_member', $id_member);
		$query = $this->db->get();
		$data = $query->result_array();

		$result = array(
			'data' => $data,
		);

		return $result;
	}

  //INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		//$insert_id = $this->db->insert_id();
		return $res;
	}

	//DELETE
	public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}
}
