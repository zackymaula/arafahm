<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Admin_Slide extends CI_Model {

    //ADMIN LIST
	public function GetSlide()
	{
		$this->db->select('*');
		$this->db->from('c_sliders');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN UPDATE DETAIL
	public function GetSlideDetail($id_slide)
	{
		$this->db->select('*');
		$this->db->from('c_sliders');
		$this->db->where('id_slide', $id_slide);
		$query = $this->db->get();
		return $query->result_array();
	}
    
    //ADMIN INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//ADMIN DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//ADMIN UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
    }
    
}