<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {

    public function template_back($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('back/v_template_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('back/v_template_footer', $vars, $return);

        return $content;
        else:
        $this->view('back/v_template_header', $vars);
        $this->view($template_name, $vars);
        $this->view('back/v_template_footer', $vars);
        endif;
    }
}