<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	public function index()
	{
		$data = $this->m_api_promo->GetPromo();
		echo json_encode($data);
  }
}
