<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide extends CI_Controller {

	public function index()
	{
		$data = $this->m_api_slide->GetSlide();
		echo json_encode($data);
    }
}