<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	// public function login()
	// {
	// 	$member_generate_id = 'LcStdoSv8xaQ9qbXwEZDsasgGHx2';
	//   $member_phone = '+629876543210';
	//   $member_name = '';
	//   $member_location_url = '';
	//   $member_location_full = '';
	// 	// $member_generate_id = $_POST['member_generate_id'];
	//   // $member_phone = $_POST['member_phone'];
	//   // $member_name = $_POST['member_name'];
	//   // $member_location_url = $_POST['member_location_url'];
	//   // $member_location_full = $_POST['member_location_full'];
	// 	$created_at = date('Y-m-d H:i:s');
	// 	$updated_at = date('Y-m-d H:i:s');
	//
	// 	$dataMember = $this->m_api_auth->GetMembers($member_generate_id,$member_phone);
	// 	$arrayDataMember = $dataMember->result_array();
	//
	// 	if ($dataMember->num_rows()>=1) {
	//
	// 		//JIKA MEMBER SUDAH ADA
	// 		$data_update_member = array(
	// 			'member_nama_depan' => $member_name,
	// 			'member_location_url' => $member_location_url,
	// 			'member_location_full' => $member_location_full,
	// 			'updated_at' => $updated_at,
	// 		);
	// 		$arrayWhere = array(
	// 			'member_generate_id' => $member_generate_id,
	// 			'member_phone' => $member_phone
	// 		);
	// 		$this->m_api_auth->Update('u_members',$data_update_member,$arrayWhere);
	//
	// 		foreach ($arrayDataMember as $data) {
	// 			$dataArrayConvert = array(
	// 				'id_member' => (int)$data['id_member'],
	// 				'member_generate_id' => $data['member_generate_id'],
	// 				'member_phone' => $data['member_phone'],
	// 				'member_nama_depan' => $data['member_nama_depan'],
	// 				'member_location_url' => $data['member_location_url'],
	// 				'member_location_full' => $data['member_location_full'],
	// 				'created_at' => $data['created_at'],
	// 				'updated_at' => $data['updated_at'],
	// 			);
	// 		};
	// 		// $result = array(
	// 		// 	'data' => $dataArrayConvert,
	// 		// );
	//
	// 	} else {
	// 		//JIKA DATA MEMBER BELUM ADA >>> CREATE MEMBER
	// 		$dataInsert = array(
	// 			'member_generate_id' => $member_generate_id,
	// 			'member_phone' => $member_phone,
	// 			'member_nama_depan' => $member_name,
	// 			'member_location_url' => $member_location_url,
	// 			'member_location_full' => $member_location_full,
	// 			'created_at' => $created_at,
	// 			'updated_at' => '',
	// 		);
	//
	// 		//START INSERT
	// 		$this->db->trans_start();
	//
	// 		//INSERT MEMBER + GET ID MEMBER
	// 		$insert_id_member = $this->m_api_auth->Insert('u_members',$dataInsert);
	//
	// 		$dataArrayConvert = array(
	// 			'id_member' => (int)$insert_id_member,
	// 			'member_generate_id' => $member_generate_id,
	// 			'member_phone' => $member_phone,
	// 			'member_nama_depan' => $member_name,
	// 			'member_location_url' => $member_location_url,
	// 			'member_location_full' => $member_location_full,
	// 			'created_at' => $created_at,
	// 			'updated_at' => '',
	// 		);
	//
	// 		//STOP INSERT
	// 		$this->db->trans_complete();
	//
	// 		// $result = array(
	// 		// 	'data' => $dataArrayConvert,
	// 		// );
	// 	};
	//
	// 	$result = $dataArrayConvert;
	//
	// 	echo json_encode($result);
	// }

	//CHECK MEMBER ADA ATAU TIDAK
	// public function check()
	// {
	// 	$generate_key = 'KJSoIsjdioajIOjOiJAOsidjisOIJ+space++62921292991';
	// 	list($member_generate_id, $member_phone) = explode("+space+", $generate_key);
	//
	// 	//$member_generate_id = 'LcStdoSv8xaQ9qbXwEZDsasgGHx2';
	// 	//$member_phone = '+629876543210';
	// 	// $member_generate_id = $_POST['member_generate_id'];
	// 	// $member_phone = $_POST['member_phone'];
	//
	// 	$status = $this->m_api_auth->CheckMembers($member_generate_id,$member_phone);
	// 	echo json_encode($status);
	// }

	//GET DATA PADA PAGE USER MAIN
	public function get()
	{
		// $member_generate_id = '5ibJiNXmP0QvCVvuLvGXbEYDBAD3';
		// $member_phone = '+85298765432';
		$member_generate_id = $_POST['member_generate_id'];
		$member_phone = $_POST['member_phone'];

		$arrarData = $this->m_api_auth->GetMembers($member_generate_id,$member_phone)->result_array();

		foreach ($arrarData as $data) {
			$return = array(
				'id_member' => $data['id_member'],
				'member_generate_id' => $data['member_generate_id'],
				'member_phone' => $data['member_phone'],
				'member_nama_depan' => $data['member_nama_depan'],
				'member_nama_belakang' => $data['member_nama_belakang'],
				'member_tgl_lahir' => $data['member_tgl_lahir'],
				//'member_photo_id_card' => $data['member_photo_id_card'],
				'member_fb_name' => $data['member_fb_name'],
				'member_fb_email' => $data['member_fb_email'],
				//'member_fb_phone' => $data['member_fb_phone'],
				'member_fb_photo' => $data['member_fb_photo'],
				//'member_location_url' => $data['member_location_url'],
				//'member_location_full' => $data['member_location_full'],
				'created_at' => $data['created_at'],
				'updated_at' => $data['updated_at'],
			);
		}

		echo json_encode($return);
	}

	//CHECK MEMBER ADA ATAU TIDAK KETIKA LOGIN
	public function check()
	{
		// $member_phone = '+85298765432';
		$member_phone = $_POST['member_phone'];

		$arrarData = $this->m_api_auth->CheckMembers($member_phone);

		// foreach ($arrarData as $data) {
		// 	$return = array(
		// 		'status' => 'success',
		// 	);
		// }
		if ($arrarData->num_rows()>=1) {
			$return = array(
				'status' => 'success', //JIKA ADA
			);
		} else {
			$return = array(
				'status' => 'failed', //JIKA TIDAK ADA
			);
		}

		echo json_encode($return);
	}

	//JIKA BELUM PUNYA AKUN, AKAN INSERT
	public function register()
	{
		$member_generate_id = $_POST['member_generate_id'];
	  $member_phone = $_POST['member_phone'];
	  $member_nama_depan = $_POST['member_nama_depan'];
	  $member_nama_belakang = $_POST['member_nama_belakang'];
	  $member_tgl_lahir = $_POST['member_tgl_lahir'];
	  $member_photo_id_card = $_POST['member_photo_id_card'];
	  $member_fb_name = $_POST['member_fb_name'];
	  $member_fb_email = $_POST['member_fb_email'];
	  $member_fb_phone = $_POST['member_fb_phone'];
	  $member_fb_photo = $_POST['member_fb_photo'];
	  $member_location_url = $_POST['member_location_url'];
	  $member_location_full = $_POST['member_location_full'];
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		//UPLOAD FILE ID CARD
		// $decode = base64_decode($member_photo_id_card);
		// $file_name = 'member-id-card-'.$member_generate_id.date('ymdHis').'.png';
		// $file_path = 'assets/images/members/';
		// $file = $file_path.$file_name;
		$file = '';

		$dataInsert = array(
			'member_generate_id' => $member_generate_id,
			'member_phone' => $member_phone,
			'member_nama_depan' => $member_nama_depan,
			'member_nama_belakang' => $member_nama_belakang,
			'member_tgl_lahir' => $member_tgl_lahir,
			'member_photo_id_card' => $file,
			'member_fb_name' => $member_fb_name,
			'member_fb_email' => $member_fb_email,
			'member_fb_phone' => $member_fb_phone,
			'member_fb_photo' => $member_fb_photo,
			'member_location_url' => $member_location_url,
			'member_location_full' => $member_location_full,
			'created_at' => $created_at,
			'updated_at' => '',
		);

		$this->m_api_auth->Insert('u_members',$dataInsert);

		//TAHAP 1 | UPLOAD PHOTO
		// $filename = $file; // output file name
		// $fileData = $decode;
		//
		// $im = imagecreatefromstring($fileData);

		// TAHAP 2 | COMPRESS <<<<<<<<<<<<<<<<<<<<
		// $source_width = imagesx($im);
		// $source_height = imagesy($im);
		// $ratio =  $source_height / $source_width;
		//
		// $new_width = 300; // assign new width to new resized image
		// $new_height = $ratio * 300;
		//
		// $thumb = imagecreatetruecolor($new_width, $new_height);
		//
		// $transparency = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
		// imagefilledrectangle($thumb, 0, 0, $new_width, $new_height, $transparency);
		//
		// imagecopyresampled($thumb, $im, 0, 0, 0, 0, $new_width, $new_height, $source_width, $source_height);
		// imagepng($thumb, $filename, 9);
		// imagedestroy($im);

		// TAHAP 2 | CROP IMAGE <<<<<<<<<<<<<<<<<<<<<
		// $size = min(imagesx($im), imagesy($im));
		// $source_height = imagesy($im);
		// $y = $source_height/5;
		// $im2 = imagecrop($im, ['x' => 0, 'y' => $y, 'width' => $size, 'height' => $size]);
		// if ($im2 !== FALSE) {
		//     imagepng($im2, $filename);
		//     imagedestroy($im2);
		// }
		// imagedestroy($im);

	}
}
