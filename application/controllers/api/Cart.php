<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

  public function index()
	{
    // $member_generate_id = 'LcStdoSv8xaQ9qbXwEZDsasgGHx2';
    // $member_phone = '+629876543210';
    $member_generate_id = $_POST['member_generate_id'];
    $member_phone = $_POST['member_phone'];

    $id_member = $this->m_api_cart->GetIdMember($member_generate_id,$member_phone);

		$data = $this->m_api_cart->GetCartPerMember($id_member);
		echo json_encode($data);
  }

  public function insert()
  {
    $member_generate_id = $_POST['member_generate_id'];
    $member_phone = $_POST['member_phone'];
    $id_product = $_POST['id_product'];
    $id_promo = $_POST['id_promo'];
    $created_at = date('Y-m-d H:i:s');

    $id_member = $this->m_api_cart->GetIdMember($member_generate_id,$member_phone);

    $dataInsert = array(
      'id_member' => $id_member,
      'id_product' => $id_product,
      'id_promo' => $id_promo,
      'created_at' => $created_at,
      'updated_at' => '',
    );

    $this->m_api_cart->Insert('t_cart',$dataInsert);
  }

  public function delete($id_cart)
	{
		//DELETE
		$where = array('id_cart' => $id_cart);
		$query = $this->m_api_cart->Delete('t_cart',$where);
	}
}
