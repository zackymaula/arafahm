<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	//API ALL
	public function all()
	{
		$data = $this->m_api_product->GetProductAll();
		echo json_encode($data);
	}

	//API ALL
	public function limit()
	{
		$data = $this->m_api_product->GetProductAllLimit();
		echo json_encode($data);
	}

	//API ONE DETAIL
	public function detail($id_products)
	{
		$data = $this->m_api_product->GetProductDetail($id_products);
		echo json_encode($data);
	}

	//PER CATEGORY
	public function category($id_categories)
	{
		$data = $this->m_api_product->GetProductPerCategory($id_categories);
		echo json_encode($data);
	}

	//PER SUB CATEGORY
	public function categorysub($id_categories, $id_category_sub)
	{
		$data = $this->m_api_product->GetProductPerCategorySub($id_categories, $id_category_sub);
		echo json_encode($data);
  }
	
}
