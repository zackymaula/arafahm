<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function index()
	{
		$data = $this->m_api_category->GetCategory();
		echo json_encode($data);
	}
	
	public function sub($id_categories)
	{
		$data = $this->m_api_category->GetCategorySub($id_categories);
		echo json_encode($data);
    }
}