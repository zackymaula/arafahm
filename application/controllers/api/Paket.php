<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller {

	public function index()
	{
		$data = $this->m_api_paket->GetPaket();
		echo json_encode($data);
    }
}