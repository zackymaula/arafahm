<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function index()
	{
		$dataProductList = $this->m_admin_product->GetProductList();
		$arrayData = array(
			'data_product' => $dataProductList
		);

		$this->load->template_back('back/v_product', $arrayData);
	}

	public function insert()
	{
		$dataCategories = $this->m_admin_product->GetAllCategory();
		$dataCategorySub = $this->m_admin_product->GetAllCategorySub();

		$template_data = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub
		);

		$this->load->template_back('back/v_product_insert', $template_data);
	}

	public function insert_do()
	{
		$product_id_category = $_POST['post_category'];
		$product_id_sub_category = $_POST['post_sub_category'];
		$product_name = $_POST['post_name'];
		$product_price_hk = $_POST['post_price_hk'];
		$product_description = $_POST['post_description'];
		$created_at = date('Y-m-d H:i:s');

		//UPLOAD PRODUCT FILES
		if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
      $ext = pathinfo($file, PATHINFO_EXTENSION);
      $file_name = 'product-'.date('ymdHis').'.'.$ext;
      $file_path = 'assets/images/products/';

      $config['upload_path'] = $file_path;
      $config['allowed_types'] = 'jpg|jpeg|png';
      $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_product = array(
			'product_name' => $product_name,
			'product_price_hk' => $product_price_hk,
			'product_file_path' => $file_path,
			'product_file_name' => $file_name,
			'description' => $product_description,
			'created_at' => $created_at
		);

		//START INSERT
		$this->db->trans_start();

		//INSERT PRODUCT
		$insert_product_id = $this->m_admin_product->InsertProduct('p_products',$data_product);

		//INSERT PRODUCT CATEGORY
		$data_product_category = array(
			'id_products' => $insert_product_id,
			'id_categories' => $product_id_category,
			'id_category_sub' => $product_id_sub_category,
			'created_at' => $created_at
		);
		$this->m_admin_product->InsertProductCategory('p_product_categories',$data_product_category);

		$this->db->trans_complete();
		//STOP INSERT

		redirect('admin/product');
	}

	public function detail($id_products)
	{
		$dataCategories = $this->m_admin_product->GetAllCategory();
		$dataCategorySub = $this->m_admin_product->GetAllCategorySub();
		$dataProductDetail = $this->m_admin_product->GetProductDetail($id_products);
		$template_data = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub,
			'id_products' => $dataProductDetail[0]['id_products'],
			'c_name' => $dataProductDetail[0]['c_name'],
			'cs_name' => $dataProductDetail[0]['cs_name'],
			'product_name' => $dataProductDetail[0]['product_name'],
			'price_hk' => $dataProductDetail[0]['product_price_hk'],
			'product_file_path' => $dataProductDetail[0]['product_file_path'],
			'product_file_name' => $dataProductDetail[0]['product_file_name'],
			'description' => $dataProductDetail[0]['description']
		);

		$this->load->template_back('back/v_product_detail', $template_data);
	}

	public function delete($id_products)
	{
		//DELETE PRODUCT FILE
		$dataFile = $this->m_admin_product->GetProductDetail($id_products);
		foreach ($dataFile as $data) {
			$file_path = $data['product_file_path'];
			$file_name = $data['product_file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_products' => $id_products);
		$query = $this->m_admin_product->ProductDelete('p_products',$where);
		$query = $this->m_admin_product->ProductDelete('p_product_categories',$where);

		if ($query >= 1) {
			redirect('admin/product');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_products)
	{
		$dataCategories = $this->m_admin_product->GetAllCategory();
		$dataCategorySub = $this->m_admin_product->GetAllCategorySub();
		$dataProductDetail = $this->m_admin_product->GetProductDetail($id_products);
		$template_data = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub,
			'id_products' => $dataProductDetail[0]['id_products'],
			'id_categories' => $dataProductDetail[0]['id_categories'],
			'id_category_sub' => $dataProductDetail[0]['id_category_sub'],
			'product_name' => $dataProductDetail[0]['product_name'],
			'price_hk' => $dataProductDetail[0]['product_price_hk'],
			'product_file_path' => $dataProductDetail[0]['product_file_path'],
			'product_file_name' => $dataProductDetail[0]['product_file_name'],
			'description' => $dataProductDetail[0]['description']
		);

		$this->load->template_back('back/v_product_update', $template_data);
	}

	public function update_do()
	{
		$product_id_products = $_POST['post_id_products'];
		$product_id_category = $_POST['post_category'];
		$product_id_sub_category = $_POST['post_sub_category'];
		$product_name = $_POST['post_name'];
		$product_price_hk = $_POST['post_price_hk'];
		$product_description = $_POST['post_description'];
		$product_file_path = $_POST['post_file_path'];
		$product_file_name = $_POST['post_file_name'];
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data_update_product = array(
			'product_name' => $product_name,
			'product_price_hk' => $product_price_hk,
			'description' => $product_description,
			'updated_at' => $updated_at
		);

		$data_update_product_category = array(
			'id_categories' => $product_id_category,
			'id_category_sub' => $product_id_sub_category,
			'updated_at' => $updated_at
		);

		if(!empty($_FILES['post_file']['name'])){

            $config['upload_path'] = $product_file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $product_file_name;
            $config['overwrite'] = TRUE;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$where = array('id_products' => $product_id_products);

		$query = $this->m_admin_product->ProductUpdate('p_products',$data_update_product,$where);
		$query = $this->m_admin_product->ProductUpdate('p_product_categories',$data_update_product_category,$where);

		if ($query >= 1) {
			redirect('admin/product');
		} else {
			echo "Update Data Gagal";
		}
	}
}
