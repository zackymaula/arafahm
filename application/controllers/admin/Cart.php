<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function index()
	{
		$dataMemberList = $this->m_admin_cart->GetCartList();
		$arrayData = array(
			'data_cart' => $dataMemberList
		);

		$this->load->template_back('back/v_cart', $arrayData);
	}

}
