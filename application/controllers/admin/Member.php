<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function index()
	{
		$dataMemberList = $this->m_admin_member->GetMemberList();
		$arrayData = array(
			'data_member' => $dataMemberList
		);

		$this->load->template_back('back/v_member', $arrayData);
	}

	public function detail($id_member)
	{
		$dataMember = $this->m_admin_member->GetMember($id_member);
		$dataCartPerMember = $this->m_admin_member->GetCharPerMember($id_member);

		$arrayData = array(
			'id_member' => $dataMember[0]['id_member'],
			'member_name' => $dataMember[0]['member_name'],
			'member_phone' => $dataMember[0]['member_phone'],
			'data_cart' => $dataCartPerMember,
		);

		$this->load->template_back('back/v_member_detail', $arrayData);
	}
}
