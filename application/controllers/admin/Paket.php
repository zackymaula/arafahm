<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller {

	public function index()
	{	
		$dataPaketList = $this->m_admin_paket->GetPaket();
		$arrayData = array(
			'data_paket' => $dataPaketList
		);

		$this->load->template_back('back/v_paket', $arrayData);
	}

	public function insert()
	{
		$this->load->template_back('back/v_paket_insert');
	}

	public function insert_do()
	{
		$created_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = 'paket-'.date('ymdHis').'.'.$ext;
            $file_path = 'assets/images/paket/';

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_paket = array(
			'file_path' => $file_path,
			'file_name' => $file_name,
			'created_at' => $created_at
		);

		$this->m_admin_paket->Insert('c_paket',$data_paket);

		redirect('admin/paket');
	}

	public function delete($id_paket)
	{
		//DELETE FILE
		$dataFile = $this->m_admin_paket->GetPaketDetail($id_paket);
		foreach ($dataFile as $data) {
			$file_path = $data['file_path'];
			$file_name = $data['file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_paket' => $id_paket);
		$query = $this->m_admin_paket->Delete('c_paket',$where);

		if ($query >= 1) {
			redirect('admin/paket');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_paket)
	{
		$dataPaketDetail = $this->m_admin_paket->GetPaketDetail($id_paket);
		$template_data = array(
			'id_paket' => $dataPaketDetail[0]['id_paket'],
			'file_path' => $dataPaketDetail[0]['file_path'],
			'file_name' => $dataPaketDetail[0]['file_name']
		);

		$this->load->template_back('back/v_paket_update', $template_data);
	}

	public function update_do()
	{
		$id_paket = $_POST['post_id_paket'];
		$file_path = $_POST['post_file_path'];
		$file_name = $_POST['post_file_name'];
		$updated_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;
            $config['max_size'] = 500;
            $config['overwrite'] = TRUE;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_update_slide = array(
			'updated_at' => $updated_at
		);

		$where = array('id_paket' => $id_paket);		
		$query = $this->m_admin_paket->Update('c_paket',$data_update_slide,$where);

		if ($query >= 1) {
			redirect('admin/paket');
		} else {
			echo "Update Data Gagal";
		}
	}
}