<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dev extends CI_Controller {

	public function index()
	{
		redirect('dev/maintenance');
	}

	public function admin()
	{
		$this->load->view('back/v_login');
	}

	public function main()
	{
        $this->load->view('dev/v_main');
	}

	public function error()
	{
		$this->load->view('dev/v_error');
	}
}
