<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

  public function index()
	{
    $member_generate_id = $_POST['member_generate_id'];
    $member_phone = $_POST['member_phone'];

    $id_member = $this->m_api_mg_cart->GetIdMember($member_generate_id,$member_phone);

		$data = $this->m_api_mg_cart->GetCartPerMember($id_member);
		echo json_encode($data);
  }

  public function insert()
  {
    $member_generate_id = $_POST['member_generate_id'];
    $member_phone = $_POST['member_phone'];
    $catalog_id = $_POST['post_id'];
    $catalog_name = $_POST['post_name'];
    $catalog_sku = $_POST['post_sku'];
    $catalog_price = $_POST['post_price'];
    $catalog_description = $_POST['post_description'];
    $catalog_image = $_POST['post_image'];
    $catalog_categorynames = $_POST['post_categorynames'];
    $catalog_categorysubnames = $_POST['post_categorysubnames'];
    $created_at = date('Y-m-d H:i:s');

    $id_member = $this->m_api_mg_cart->GetIdMember($member_generate_id,$member_phone);

    $dataInsert = array(
      'id_member' => $id_member,
      'catalog_id' => $catalog_id,
      'catalog_name' => $catalog_name,
      'catalog_sku' => $catalog_sku,
      'catalog_price' => $catalog_price,
      'catalog_description' => $catalog_description,
      'catalog_image' => $catalog_image,
      'catalog_categorynames' => $catalog_categorynames,
      'catalog_categorysubnames' => $catalog_categorysubnames,
      'created_at' => $created_at,
      'updated_at' => '',
    );

    $this->m_api_mg_cart->Insert('t_cart_mg',$dataInsert);
  }

  public function delete($id_cart)
	{
		//DELETE
		$where = array('id_cart' => $id_cart);
		$query = $this->m_api_mg_cart->Delete('t_cart_mg',$where);
	}
}
