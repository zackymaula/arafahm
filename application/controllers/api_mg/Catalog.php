<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

	//MAGENTO CATALOG ALL
	public function all()
	{
		$data = $this->m_api_mg_catalog->GetMagentoCatalogAll();
		echo json_encode($data);
	}

	//MAGENTO CATALOG CATEGORY
	public function category()
	{
		//$category = 'Gadget';

    $category = $_POST['post_category'];

		$data = $this->m_api_mg_catalog->GetMagentoCatalogCategory($category);
		echo json_encode($data);
	}

  //MAGENTO CATALOG CATEGORY SUB
	public function categorysub()
	{
    // $category = 'Elektronik';
    // $categorysub = 'Rak TV';

    $category = $_POST['post_category'];
    $categorysub = $_POST['post_categorysub'];

		$data = $this->m_api_mg_catalog->GetMagentoCatalogCategorySub($category,$categorysub);
		echo json_encode($data);
	}

}
