<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="content-heading clearfix">
      <div class="heading-left">
        <h1 class="page-title">Keranjang</h1>
        <p class="page-subtitle">List Keranjang</p>
      </div>
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active">Keranjang</li>
      </ul>
    </div>
    <div class="container-fluid">
      <!-- FEATURED DATATABLE -->
      <!-- <p class="demo-button">
        <a href="<?php //echo base_url(); ?>admin/slide/insert" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Insert"><i class="fa fa-plus-square"></i>
          <span class="sr-only">Insert</span>
        </a>
      </p> -->
      <div class="table-responsive">
        <table id="featured-datatable" class="table table-striped table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Member</th>
              <th>Nomor Handphone</th>
              <th>Nama Produk di Keranjang
              </th>
              <th>Tanggal</th>
              <!-- <th>Option</th> -->
            </tr>
          </thead>
          <tbody>
            <?php $no = 1;
            foreach ($data_cart as $data_cart) { ?>
            <tr>
              <td><?php echo $no ?></td>
              <td><?php echo $data_cart['member_name']; ?></td>
              <td><?php echo $data_cart['member_phone']; ?></td>
              <td><?php echo $data_cart['product_name']; ?>
                <a href="<?php echo base_url(); ?>admin/member/detail/<?php echo $data_cart['id_member'] ?>" type="button" class="btn btn-info btn-sm"><i class="fa ti-shopping-cart"></i>
									<span>Detail</span>
								</a>
              </td>
              <td><?php echo $data_cart['date']; ?></td>
              <!-- <td>
                <button type="button" class="btn btn-info btn-sm"><i class="fa ti-shopping-cart"></i>
									<span>Keranjang</span>
								</button>
              </td> -->
            </tr>
            <?php $no++; }; ?>
          </tbody>
        </table>
      </div>
      <!-- END FEATURED DATATABLE -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
