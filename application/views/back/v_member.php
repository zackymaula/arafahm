<div class="main">
  <!-- MAIN CONTENT -->
  <div class="main-content">
    <div class="content-heading clearfix">
      <div class="heading-left">
        <h1 class="page-title">Member</h1>
        <p class="page-subtitle">List Member</p>
      </div>
      <ul class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="active">Member</li>
      </ul>
    </div>
    <div class="container-fluid">
      <!-- FEATURED DATATABLE -->
      <!-- <p class="demo-button">
        <a href="<?php //echo base_url(); ?>admin/slide/insert" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Insert"><i class="fa fa-plus-square"></i>
          <span class="sr-only">Insert</span>
        </a>
      </p> -->
      <div class="table-responsive">
        <table id="featured-datatable" class="table table-striped table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Member</th>
              <th>No. Handphone</th>
              <th>Alamat</th>
              <th>Location</th>
              <!-- <th>Option</th> -->
            </tr>
          </thead>
          <tbody>
            <?php $no = 1;
            foreach ($data_member as $data_member) { ?>
            <tr>
              <td><?php echo $no ?></td>
              <td><?php echo $data_member['member_name']; ?></td>
              <td><?php echo $data_member['member_phone']; ?></td>
              <td><?php echo $data_member['member_location_full']; ?></td>
              <td><a href="<?php echo $data_member['member_location_url']; ?>" target="_blank"><?php echo $data_member['member_location_url']; ?></a></td>
              <!-- <td>
                <a href="<?php //echo base_url(); ?>admin/member/detail/<?php //echo $data_member['id_member'] ?>" type="button" class="btn btn-info btn-sm"><i class="fa ti-shopping-cart"></i>
									<span>Keranjang</span>
								</a>
              </td> -->
            </tr>
            <?php $no++; }; ?>
          </tbody>
        </table>
      </div>
      <!-- END FEATURED DATATABLE -->
    </div>
  </div>
  <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
