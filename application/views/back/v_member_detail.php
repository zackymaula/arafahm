
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Member Detail</h1>
							<p class="page-subtitle">Detail a Member</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							<li class="active">Member Detail</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-4 col-lg-4">
								<!-- SUBMIT PRODUCTS -->
                <div class="panel project-item">
									<div class="panel-heading">
										<h2 class="panel-title"><a href="#">Member</a></h2>
                    <div class="right">
                      <i class="fa ti-user"></i>
                    </div>
									</div>
									<div class="panel-body">
										<div class="left">
											<div class="info">
												<span class="title">Nama</span>
												<span class="value"><? echo $member_name ?></span>
											</div>
											<div class="info">
												<span class="title">Nomor Handphone</span>
												<span class="value"><? echo $member_phone ?></span>
											</div>
										</div>
									</div>
								</div>

							</div>

              <div class="col-md-8 col-lg-8">
								<!-- SUBMIT PRODUCTS -->
                <div class="panel project-item">
									<div class="panel-heading">
										<h2 class="panel-title"><a href="#">Keranjang</a></h2>
                    <div class="right">
                      <i class="fa ti-shopping-cart"></i>
                    </div>
									</div>
									<div class="panel-body">
                    <div class="table-responsive">

                      <table id="featured-datatable" class="table table-striped table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Picture</th>
                            <th>Nama Produk</th>
                            <th>Harga</th>
                            <th>Tanggal</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no = 1;
                          foreach ($data_cart as $data_cart) { ?>
                          <tr>
                            <td><?php echo $no ?></td>
                            <td><img src="<?php echo base_url().$data_cart['product_file_path'].$data_cart['product_file_name']; ?>" style="width: 80px;" class="w3-border w3-padding" alt="Image"></td>
        										<td><?php echo $data_cart['product_name']; ?></td>
                            <td>
        											<p><b><?php echo ($data_cart['product_price_hk']=='')?'0':$data_cart['product_price_hk'] ?></b> HKD</p>
        										</td>
        										<td><?php echo $data_cart['date']; ?></td>
                          </tr>
                          <?php $no++; }; ?>

                        </tbody>
                      </table>
                    </div>

									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
