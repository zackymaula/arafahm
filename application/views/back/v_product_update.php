
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Product Update</h1>
							<p class="page-subtitle">Update a Product</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/product">Product</a></li>
							<li class="active">Product Update</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT PRODUCTS -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Update a Product</h3>
									</div>
									<div class="panel-body">
										<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/product/update_do" novalidate enctype="multipart/form-data">

												<input type="hidden" name="post_id_products" value="<?php echo $id_products ?>" />
												<input type="hidden" name="post_file_path" value="<?php echo $product_file_path ?>" />
												<input type="hidden" name="post_file_name" value="<?php echo $product_file_name ?>" />

												<div class="form-group">
													<label for="ppreview-image" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php echo base_url().$product_file_path.$product_file_name; ?>" id="preview-image" style="width: 300px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div>

												<div class="form-group">
													<label for="product-pictures-150" class="col-sm-3 control-label">Pictures</label>
													<div class="col-md-9">
														<input type="file" id="upload-pictures-150" name="post_file" accept=".jpg,.jpeg,.png">
														<p class="help-block">
															<em>Valid file type: .jpg, .jpeg, .png. File size max: 150 KB. File dimension: 500x500 pixel</em>
														</p>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Category</label>
													<div class="col-md-9">
														<select id="category" class="form-control" name="post_category" required>
															<option value="">Please Select</option>
															<?php foreach ($data_categories as $data_categories) {
																if ($data_categories['id_categories']=='1' || $data_categories['id_categories']=='7') {
																} else { ?>
																<option <?php if( $id_categories==$data_categories['id_categories']){echo "selected"; } ?>
																	value="<?php echo $data_categories['id_categories'] ?>"><?php echo $data_categories['name'] ?>
																</option>
															<?php }}; ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Sub Category</label>
													<div class="col-md-9">
														<select id="sub_category" class="form-control" name="post_sub_category">
															<option value="">Please Select</option>
															<?php foreach ($data_category_sub as $data_category_sub) { ?>
																<option <?php if( $id_category_sub==$data_category_sub['id_category_sub']){echo "selected"; } ?>
																	id="sub_category" class="<?php echo $data_category_sub['id_categories'] ?>" value="<?php echo $data_category_sub['id_category_sub'] ?>"><?php echo $data_category_sub['name'] ?>
																</option>
															<?php }; ?>
														</select>
													</div>
												</div>

												<div class="form-group">
													<label for="product-name" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="product-name" name="post_name" value="<?php echo $product_name ?>" placeholder="Name" required>
													</div>
												</div>

												<div class="form-group">
													<label for="product-price-hk" class="col-sm-3 control-label">Price Hongkong</label>
													<div class="col-sm-2">
														<div class="input-group">
															<input type="text" class="form-control" id="product-price-hk" name="post_price_hk" value="<?php echo $price_hk ?>" placeholder="0" required>
															<span class="input-group-addon">HKD</span>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="product-description" class="col-sm-3 control-label">Description</label>
													<div class="col-sm-9">
														<textarea class="summernote_description" id="product-description" name="post_description"><?php echo $description ?>
														</textarea>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Update Product</button>
													</div>
												</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
