			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Promo</h1>
							<p class="page-subtitle">List Promo</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboard</a></li>
							<li class="active">Promo</li>
						</ul>
					</div>
					<div class="container-fluid">
						<!-- FEATURED DATATABLE -->
						<p class="demo-button">
							<a href="<?php echo base_url(); ?>admin/promo/insert" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Insert"><i class="fa fa-plus-square"></i>
								<span class="sr-only">Insert</span>
							</a>
						</p>
						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Picture</th>
										<th>Price</th>
										<th>Title</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_promo as $data_promo) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><img src="<?php echo base_url().$data_promo['promo_file_path'].$data_promo['promo_file_name']; ?>" style="width: 150px;" class="w3-border w3-padding" alt="Image"></td>
										<td>
											<p><b><?php echo ($data_promo['promo_price_hk']=='')?'0':$data_promo['promo_price_hk'] ?></b> HKD</p>
										</td>
										<td><?php echo $data_promo['promo_name']; ?></td>
										<td>
											<div class="btn-group">
												<!-- <a href="#" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Detail">
													<span class="sr-only">Detail</span><i class="fa fa-info-circle"></i></a> -->
												<a href="<?php echo base_url(); ?>admin/promo/update/<?php echo $data_promo['id_promo'] ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
													<span class="sr-only">Edit</span><i class="fa fa-pencil"></i> Edit</a>
												<a href="<?php echo base_url(); ?>admin/promo/delete/<?php echo $data_promo['id_promo'] ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Remove" onclick="return confirm('Anda yakin untuk menghapus data?')">
													<span class="sr-only">Remove</span><i class="fa fa-remove"></i> Delete</a>
											</div>
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
						<!-- END FEATURED DATATABLE -->
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
