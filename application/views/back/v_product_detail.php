
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Product Detail</h1>
							<p class="page-subtitle">Detail a Product</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/product">Product</a></li>
							<li class="active">Product Detail</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT PRODUCTS -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Detail a Product</h3>
									</div>
									<div class="panel-body">

												<input type="hidden" name="post_id_products" value="<?php echo $id_products ?>" />

												<div class="form-group">
													<label for="product-file" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php echo base_url().$product_file_path.$product_file_name; ?>" style="width: 300px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div>

												<div class="form-group">
													<label for="category" class="col-sm-3 control-label">Category</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="category" name="post_category" value="<?php echo $c_name ?>" placeholder="-" disabled>
													</div>
												</div>
												<div class="form-group">
													<label for="sub-category" class="col-sm-3 control-label">Sub Category</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="sub-category" name="post_sub_category" value="<?php echo $cs_name ?>" placeholder="-" disabled>
													</div>
												</div>
												<div class="form-group">
													<label for="product-name" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="product-name" name="post_name" value="<?php echo $product_name ?>" placeholder="-" disabled>
													</div>
												</div>

												<div class="form-group">
													<label for="product-price-hk" class="col-sm-3 control-label">Price Hongkong</label>
													<div class="col-sm-9">
														<div class="input-group">
															<input type="text" class="form-control" id="product-price-hk" name="post_price_hk" value="<?php echo $price_hk ?>" placeholder="-" disabled>
															<span class="input-group-addon">HKD</span>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="product-description" class="col-sm-3 control-label">Description</label>
													<div class="col-sm-9">
														<?php echo $description ?>
														<!-- <textarea class="form-control" placeholder="textarea" rows="4" disabled></textarea> -->
													</div>
												</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
