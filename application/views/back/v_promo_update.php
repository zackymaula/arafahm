
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Promo Update</h1>
							<p class="page-subtitle">Update a Promo</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/promo">Promo</a></li>
							<li class="active">Promo Update</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Update a Promo</h3>
									</div>
									<div class="panel-body">
										<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/promo/update_do" enctype="multipart/form-data">

												<input type="hidden" name="post_id_promo" value="<?php echo $id_promo ?>" />
												<input type="hidden" name="post_file_path" value="<?php echo $promo_file_path ?>" />
												<input type="hidden" name="post_file_name" value="<?php echo $promo_file_name ?>" />

												<div class="form-group">
													<label for="preview-image" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php echo base_url().$promo_file_path.$promo_file_name; ?>" id="preview-image" style="width: 500px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div>

												<div class="form-group">
													<label for="upload-pictures-300" class="col-sm-3 control-label">Pictures</label>
													<div class="col-md-9">
														<input type="file" id="upload-pictures-300" name="post_file" accept=".jpg,.jpeg,.png">
														<p class="help-block">
															<em>Valid file type: .jpg, .jpeg, .png. File size max: 300 KB. File dimension: 500x500 pixel</em>
														</p>
													</div>
												</div>

												<div class="form-group">
													<label for="promo-name" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="promo-name" name="post_name" placeholder="Name" value="<?php echo $promo_name ?>" required>
													</div>
												</div>

												<div class="form-group">
													<label for="promo-price-hk" class="col-sm-3 control-label">Price Hongkong</label>
													<div class="col-sm-2">
														<div class="input-group">
															<input type="text" class="form-control" id="promo-price-hk" name="post_price_hk" value="<?php echo $promo_price_hk ?>" placeholder="0" required>
															<span class="input-group-addon">HKD</span>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="promo-description" class="col-sm-3 control-label">Description</label>
													<div class="col-sm-9">
														<textarea class="summernote_description" id="promo-description" name="post_description"><?php echo $promo_description ?>
														</textarea>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Update Promo</button>
													</div>
												</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
